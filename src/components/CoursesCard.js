import { useState, useEffect } from 'react';
import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CoursesCard({courseProp}) {

	const { name, description, price, _id } = courseProp;
	console.log(courseProp)

	return (
	<Card className="cardHighlight p-3">
	<Card.Body>
	<Card.Title className="cardTextHighlight">{name}</Card.Title>
	<Card.Text className="cardTextHighlight">Description:</Card.Text>
	<Card.Text>{description}</Card.Text>
	<Card.Text className="cardTextHighlight">Price:</Card.Text>
	<Card.Text>Php {price}</Card.Text>
	<Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>
		</Card.Body>
	</Card>
		)
}