import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ErrorMessage () {

	return(
			<Row>
				<Col className="p-5">
					<h1>Page Not Found</h1>
					<p>Go back to the <Link as={ Link } to="/" style={{ color: 'blue' }}>homepage.</Link></p>
				</Col>
			</Row>
		)
}
