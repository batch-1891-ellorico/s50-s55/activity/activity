import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	// Allows us to consume the User context object and its properties to use for our user validation
	const {user, setUser} = useContext(UserContext);
	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	function authenticate(e) {
		e.preventDefault();

		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: { 
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data=> {
			console.log(data)

			if (typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "welcome to Zuitt!"
				})
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "warning",
					text: "Check your login details and try again!"
				})
			}
		})

		// localStorage.setItem("email", email);
		// setUser({
		// email: localStorage.getItem('email')
		// })

		setEmail("");
		setPassword1("");
		// alert(`${email} has been verified. User logged in.`);
	}

	const retrieveUserDetails = (token) => {

		fetch('http://localhost:4000/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}



	useEffect(() => {
		if (email !== "" && password1 !== "")  {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password1])

	return(

		(user.id !== null) ?
		<Navigate to='/courses' />

		:

		<Form className="mt-3" onSubmit={(e) => authenticate(e)}>
		<h1 className="text-center">Login</h1>
     	<Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control 
        		type="email" 
        		placeholder="Enter email" 
        		value={email}	
        		onChange={e => {
        			setEmail(e.target.value)
        		}}
        		required
        		/>
      	</Form.Group>

      	<Form.Group className="mb-3" controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control 
        		type="password" 
        		placeholder="Password" 
        		value={password1}
        		onChange={e => {
        			setPassword1(e.target.value)
        		}}
        		required
        		/>
      	</Form.Group>
      	
      	{
      		isActive ? 
      			<Button variant="primary" type="submit" id="submitBtn">
        		Submit
      			</Button>
      		:
      			<Button variant="danger" type="submit" id="submitBtn" disabled>
        		Submit
      			</Button>
      	}

    	</Form>
		)
}


