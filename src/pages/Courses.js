import {Fragment, useEffect, useState} from 'react';
import CoursesCard from '../components/CoursesCard';
// import coursesData from '../data/coursesData';

export default function Courses() {
	// console.log(coursesData)

	const [courses, setCourses] = useState([])

	useEffect(() => {
		fetch('http://localhost:4000/courses/')
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setCourses(data.map(course => {
				return (
						<CoursesCard key={course._id} courseProp={course} />
					)
			}))
		})
	}, [])

	// const courses = coursesData.map(course => {
	// 	return (
	// 		// key is a special key attribute. Para malaman natin na ang ating key is unique with other elements
	// 			<CoursesCard key={course.id} courseProp={course}/>
	// 		)
	// })

	return(
			<Fragment>
				<h1>Courses</h1>
				{courses}
			</Fragment>
		)
}

